# K8s Agents

K8s agent repository. This agent communicates between our k8s cluster and gitlab.

## Sealed secrets

Secrets are never stored unencrypted in git. Gitlab recommends using a so-called `sealed secrets controller`. See https://docs.gitlab.com/ee/user/clusters/agent/gitops/secrets_management.html for detailed information.

Be sure to generate a group deployment token with repository read access. Also take note that the generated key name should be used as docker username and the deploy token should be used as docker password.

Generate kubernetes secret:

`kubectl create secret docker-registry gitlab-credentials --docker-server=registry.gitlab.com --docker-username=gitlab+deploy-token-<deploy-token-id> --docker-password=<deploy-token> --docker-email=admin@datastreams.io --dry-run=client -o yaml > gitlab-credentials-cluster-wide.yaml`

Seal k8s secret:

`kubeseal --format=yaml --scope=cluster-wide --cert=manifests/sealed-secrets-public.pem < gitlab-credentials-cluster-wide.yaml > gitlab-credentials-cluster-wide.sealed.yaml`
